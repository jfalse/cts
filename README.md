# cts

## evaluation

- [ ] 1h30 excercice using typescript's codeceptjs framework (1h is possible)

- [ ] clean & readable code, clean repo

- [ ] QA expertise


## TODO

- [ ] pull repository & install project (using node 18 & npm 9.6.7)

    - cd cts
    - npm install 


- [ ] write "Given When Then" gherkin syntax in an "test_agify_api.feature" in /features folder
    - think of a way to store objects from former clauses (like the "Given" one ;) )
    - reuse them in the "When" / "Then"

- [ ] implement REST http client in helper in codecept.conf.js
    - read documentation
    - call the helper in the "Given" or "When" clause

- [ ] test using your personnal name without spaces or special character:
    - http Get on https://api.agify.io/?name=<add_your_name_here>
    - ensure http response code is 200
    - ensure value type associated to key age is numeric
    - ... more test if you like !
    - test should be OK

- [ ] add steps in /step_definitions

- [ ] run & debug your code using `npx codeceptjs run`

- [ ] create merge request


## documentation

testing fw : https://codecept.io/api/#api-testing