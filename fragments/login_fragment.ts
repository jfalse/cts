import { assert } from "console";

const { I } = inject();

class LoginFragment {
  // using https://codecept.io/pageobjects/#pageobject
  user = {
    username:"me duh",
    password: 'incorrect'
  }
  constructor() {
    //insert your locators
    // this.button = '#button'
  }

  async loginAttempt(username: string, password: string) {
    I.amOnPage('/');
    I.fillField('username', username);
    I.fillField('password', password);
    I.click('Se connecter');
  }
  async isLoggedIn() {
    const kcCookie = await I.grabCookie('THE_CHOCOLATED_ONE');
    return kcCookie.includes('Test OK');
  }
}

// For inheritance
module.exports = new LoginFragment();
export = LoginFragment;
