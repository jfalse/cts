import { assert } from "console";
import LoginFragment from "../fragments/login_fragment";
const { I } = inject();
const loginFragment = new LoginFragment;
const user = loginFragment.user;

Given('a user with credentials', () => {
  assert(user.username != '');
  assert(user.password != '');
});
When('user logs in', () => {
  loginFragment.loginAttempt(user.username, user.password);
});

Then('user is logged in', () => {
  loginFragment.isLoggedIn()
});

export { };
