export const config: CodeceptJS.MainConfig = {
  require: ["ts-node/register"],
  tests: './testSuite/*.ts',
  output: './output',
  helpers: {
  },
  plugins: {
  },
  include: {
    I: './steps_file.ts',
    LoginFragemnt: "./fragments/login_fragment.ts",
  },
  gherkin: {
    features: "./features/*.feature",
    steps: [
      "./step_definitions/steps.ts"
    ]
  },
  name: 'cts'
}